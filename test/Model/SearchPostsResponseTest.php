<?php
/**
 * SearchPostsResponseTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  IlyaGum\PostsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Posts
 *
 * Ensi Posts (self-education)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace IlyaGum\PostsClient;

use PHPUnit\Framework\TestCase;

/**
 * SearchPostsResponseTest Class Doc Comment
 *
 * @category    Class
 * @description SearchPostsResponse
 * @package     IlyaGum\PostsClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class SearchPostsResponseTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "SearchPostsResponse"
     */
    public function testSearchPostsResponse()
    {
    }

    /**
     * Test attribute "data"
     */
    public function testPropertyData()
    {
    }

    /**
     * Test attribute "meta"
     */
    public function testPropertyMeta()
    {
    }
}
