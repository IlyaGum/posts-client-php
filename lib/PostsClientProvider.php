<?php

namespace IlyaGum\PostsClient;

class PostsClientProvider
{
    /** @var string[] */
    public static $apis = ['\IlyaGum\PostsClient\Api\VotesApi', '\IlyaGum\PostsClient\Api\PostsApi'];

    /** @var string[] */
    public static $dtos = [
        '\IlyaGum\PostsClient\Dto\RequestBodyOffsetPagination',
        '\IlyaGum\PostsClient\Dto\PaginationTypeOffsetEnum',
        '\IlyaGum\PostsClient\Dto\Post',
        '\IlyaGum\PostsClient\Dto\ResponseBodyCursorPagination',
        '\IlyaGum\PostsClient\Dto\PostStatusEnum',
        '\IlyaGum\PostsClient\Dto\PaginationTypeCursorEnum',
        '\IlyaGum\PostsClient\Dto\ModelInterface',
        '\IlyaGum\PostsClient\Dto\SearchPostsRequest',
        '\IlyaGum\PostsClient\Dto\RequestBodyPagination',
        '\IlyaGum\PostsClient\Dto\PatchVoteRequest',
        '\IlyaGum\PostsClient\Dto\PaginationTypeEnum',
        '\IlyaGum\PostsClient\Dto\PostReadonlyProperties',
        '\IlyaGum\PostsClient\Dto\ResponseBodyPagination',
        '\IlyaGum\PostsClient\Dto\SearchPostsFilter',
        '\IlyaGum\PostsClient\Dto\ErrorResponse2',
        '\IlyaGum\PostsClient\Dto\CreateVoteRequest',
        '\IlyaGum\PostsClient\Dto\SearchVotesResponse',
        '\IlyaGum\PostsClient\Dto\PostTypeEnum',
        '\IlyaGum\PostsClient\Dto\CreatePostRequest',
        '\IlyaGum\PostsClient\Dto\PostIncludes',
        '\IlyaGum\PostsClient\Dto\ResponseBodyOffsetPagination',
        '\IlyaGum\PostsClient\Dto\Error',
        '\IlyaGum\PostsClient\Dto\SearchVotesRequest',
        '\IlyaGum\PostsClient\Dto\PostFillableProperties',
        '\IlyaGum\PostsClient\Dto\SearchOnePostRequest',
        '\IlyaGum\PostsClient\Dto\PostLangEnum',
        '\IlyaGum\PostsClient\Dto\VoteFillableProperties',
        '\IlyaGum\PostsClient\Dto\EmptyDataResponse',
        '\IlyaGum\PostsClient\Dto\PostResponse',
        '\IlyaGum\PostsClient\Dto\ErrorResponse',
        '\IlyaGum\PostsClient\Dto\SearchVotesFilter',
        '\IlyaGum\PostsClient\Dto\Vote',
        '\IlyaGum\PostsClient\Dto\SearchPostsResponse',
        '\IlyaGum\PostsClient\Dto\Vote2',
        '\IlyaGum\PostsClient\Dto\PatchPostRequest',
        '\IlyaGum\PostsClient\Dto\SearchPostsResponseMeta',
        '\IlyaGum\PostsClient\Dto\VoteResponse',
        '\IlyaGum\PostsClient\Dto\VoteReadonlyProperties',
        '\IlyaGum\PostsClient\Dto\RequestBodyCursorPagination',
    ];

    /** @var string */
    public static $configuration = '\IlyaGum\PostsClient\Configuration';
}
