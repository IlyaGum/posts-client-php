# # SearchOnePostRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **string[]** |  | [optional] 
**include** | **string[]** |  | [optional] 
**filter** | [**\IlyaGum\PostsClient\Dto\SearchPostsFilter**](SearchPostsFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


