# # PostFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Заголовок поста | [optional] 
**content** | **string** | Содержание поста | [optional] 
**image_url** | **string** | URL главного изображения | [optional] 
**user_id** | **int** | Идентификатор автора | [optional] 
**type_id** | **int** | Идентификатор типа поста | [optional] 
**lang_id** | **int** | Идентификатор языка | [optional] 
**status_id** | **int** | Идентификатор статуса | [optional] 
**rating** | **int** | Рейтинг поста | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


