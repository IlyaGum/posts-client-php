# # SearchPostsFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор(ы) поста | [optional] 
**name** | **string** | Название поста | [optional] 
**name_like** | **string** | Название или часть названия поста | [optional] 
**code** | **string** | Код(ы) поста | [optional] 
**code_like** | **string** | Код или часть кода поста | [optional] 
**lang_id** | **int** | Идентификатор(ы) языка(ов) поста | [optional] 
**user_id** | **int** | Идентификатор(ы) автора(ов) | [optional] 
**status_id** | **int** | Идентификатор(ы) статуса(ов) поста | [optional] 
**type_id** | **int** | Идентификатор(ы) типа(ов) поста | [optional] 
**rating** | **int** | Рейтинг поста | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


