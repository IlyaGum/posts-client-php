# # SearchVotesFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор(ы) голоса | [optional] 
**post_id** | **int** | Идентификатор(ы) поста | [optional] 
**user_id** | **int** | Идентификатор(ы) пользователя | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


