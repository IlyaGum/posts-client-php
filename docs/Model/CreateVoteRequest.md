# # CreateVoteRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**post_id** | **int** | Идентификатор поста | [optional] 
**user_id** | **int** | Идентификатор пользователя | [optional] 
**is_vote_for** | **bool** | Голос \&quot;За\&quot; (true) или \&quot;Против\&quot; (false) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


