# # Post

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор поста | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления | 
**name** | **string** | Заголовок поста | [optional] 
**content** | **string** | Содержание поста | [optional] 
**image_url** | **string** | URL главного изображения | [optional] 
**user_id** | **int** | Идентификатор автора | [optional] 
**type_id** | **int** | Идентификатор типа поста | [optional] 
**lang_id** | **int** | Идентификатор языка | [optional] 
**status_id** | **int** | Идентификатор статуса | [optional] 
**rating** | **int** | Рейтинг поста | [optional] 
**votes** | [**\IlyaGum\PostsClient\Dto\Vote2[]**](Vote2.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


