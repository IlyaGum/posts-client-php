# # SearchVotesRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **string[]** |  | [optional] 
**include** | **string[]** |  | [optional] 
**pagination** | [**\IlyaGum\PostsClient\Dto\RequestBodyPagination**](RequestBodyPagination.md) |  | [optional] 
**filter** | [**\IlyaGum\PostsClient\Dto\SearchVotesFilter**](SearchVotesFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


