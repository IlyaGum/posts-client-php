# # PostIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**votes** | [**\IlyaGum\PostsClient\Dto\Vote2[]**](Vote2.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


