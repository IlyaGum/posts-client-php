# # SearchPostsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\IlyaGum\PostsClient\Dto\Post[]**](Post.md) |  | 
**meta** | [**\IlyaGum\PostsClient\Dto\SearchPostsResponseMeta**](SearchPostsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


