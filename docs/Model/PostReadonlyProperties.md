# # PostReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор поста | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


