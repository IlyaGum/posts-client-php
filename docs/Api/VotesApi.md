# IlyaGum\PostsClient\VotesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVote**](VotesApi.md#createVote) | **POST** /votes | Создание объекта типа Vote
[**deleteVote**](VotesApi.md#deleteVote) | **DELETE** /votes/{id} | Удаление объекта типа Vote
[**patchVote**](VotesApi.md#patchVote) | **PATCH** /votes/{id} | Изменение объекта типа Vote
[**searchVotes**](VotesApi.md#searchVotes) | **POST** /votes:search | Поиск объектов типа Vote



## createVote

> \IlyaGum\PostsClient\Dto\VoteResponse createVote($create_vote_request)

Создание объекта типа Vote

Создание объекта типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new IlyaGum\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_vote_request = new \IlyaGum\PostsClient\Dto\CreateVoteRequest(); // \IlyaGum\PostsClient\Dto\CreateVoteRequest | 

try {
    $result = $apiInstance->createVote($create_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->createVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_vote_request** | [**\IlyaGum\PostsClient\Dto\CreateVoteRequest**](../Model/CreateVoteRequest.md)|  |

### Return type

[**\IlyaGum\PostsClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteVote

> \IlyaGum\PostsClient\Dto\EmptyDataResponse deleteVote($id)

Удаление объекта типа Vote

Удаление объекта типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new IlyaGum\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteVote($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->deleteVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\IlyaGum\PostsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchVote

> \IlyaGum\PostsClient\Dto\VoteResponse patchVote($id, $patch_vote_request)

Изменение объекта типа Vote

Изменение объекта типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new IlyaGum\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_vote_request = new \IlyaGum\PostsClient\Dto\PatchVoteRequest(); // \IlyaGum\PostsClient\Dto\PatchVoteRequest | 

try {
    $result = $apiInstance->patchVote($id, $patch_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->patchVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_vote_request** | [**\IlyaGum\PostsClient\Dto\PatchVoteRequest**](../Model/PatchVoteRequest.md)|  |

### Return type

[**\IlyaGum\PostsClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchVotes

> \IlyaGum\PostsClient\Dto\SearchVotesResponse searchVotes($search_votes_request)

Поиск объектов типа Vote

Поиск объектов типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new IlyaGum\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_votes_request = new \IlyaGum\PostsClient\Dto\SearchVotesRequest(); // \IlyaGum\PostsClient\Dto\SearchVotesRequest | 

try {
    $result = $apiInstance->searchVotes($search_votes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->searchVotes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_votes_request** | [**\IlyaGum\PostsClient\Dto\SearchVotesRequest**](../Model/SearchVotesRequest.md)|  |

### Return type

[**\IlyaGum\PostsClient\Dto\SearchVotesResponse**](../Model/SearchVotesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

